#Author: Jacob Gildenblat, 2014
#License: you may use this for whatever you like
import sys, glob, argparse
from numpy import *
import math, cv2
import os
from scipy.stats import multivariate_normal
from functools import reduce
from scipy import linalg
import time
#from sklearn import svm
from sklearn import mixture
from sklearn.decomposition import PCA
import multiprocessing
from util import util_chunkify
import pickle

def dictionary(descriptors, n_words, pca_feature_size):

    n_desc = descriptors.shape[0]

    n_cut = 10000*n_words

    # subsample data (only need 2000 * N examples)
    if False and n_desc > n_cut:
        is_kept = arange(n_desc)
        random.shuffle (is_kept)
        is_kept = is_kept[:n_cut]
        sub_descriptors = descriptors[is_kept,:]
        print ('GMM : down-sampled input set (%d features) to %d features' % (n_desc, sub_descriptors.shape[0]))
    else:
        sub_descriptors = descriptors

    # first pass of PCA
    print ('Fitting PCA with %d components...' % pca_feature_size)
    pca = PCA (n_components=pca_feature_size)
    pca_desc = pca.fit_transform(sub_descriptors)

    # using OpenCV EM (this version generates weird results leading to singular matrices...)
    if False:
        em = cv2.ml.EM_create()
        em.setClustersNumber(N)
        em.trainEM(descriptors)
        means = float32(em.getMeans())
        covs = float32(em.getCovs())
        weights = float32(em.getWeights())[0]

    else:
        print ('Fitting GMM...(%dx%d feature set)' % (pca_desc.shape[0], pca_desc.shape[1]))
        # using scikit-learn EM (presumably faster, does not generate singular matrices)
        g = mixture.GMM(n_components=n_words, covariance_type='full')
        g.fit(pca_desc)
        means = float32(g.means_)
        covs = float32(g.covars_)
        weights = float32(g.weights_)

    #print (means.shape)
    #print (covs.shape)
    #print (weights.shape)
    return pca, means, covs, weights

def image_descriptors(file, im_r_size):
    img = cv2.imread(file)
    img = cv2.resize(img,(im_r_size,im_r_size))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    descriptors = None
    contrastThreshold = 0.08 # twice the default value in opencv
    hessianThreshold = 800 # twice the default value in opencv

    #return zeros((200,128))

#    while descriptors is None and contrastThreshold > 0.001:
#        contrastThreshold /= 2
#        sift = cv2.xfeatures2d.SIFT_create(contrastThreshold=contrastThreshold)
#        _, descriptors = sift.detectAndCompute(gray,None)
    while descriptors is None and hessianThreshold > 0.001:
        hessianThreshold /= 1.414
        surf = cv2.xfeatures2d.SURF_create(hessianThreshold=hessianThreshold, extended=False, upright=True)
        _, descriptors = surf.detectAndCompute (gray, None)

    #_ , descriptors = cv2.SIFT().detectAndCompute(cv2.cvtColor(
	#	cv2.resize(cv2.imread(file),(im_r_size,im_r_size)), cv2.COLOR_BGR2GRAY), None)

    # normalize
    #descriptors /= linalg.norm(descriptors)

    if descriptors is None:
        return zeros((1,64))

#    if descriptors is None:
#        print ('Error : feature descriptors returned None for image file %s (contrastThreshold = %.5f)' % (file, contrastThreshold))

    # sanity check on data
#    assert(not any(isnan(descriptors)))

    #print('computed %d features of size %d' % (descriptors.shape[0],descriptors.shape[1]))
    return descriptors

#def folder_descriptors(folder):
#    files = glob.glob(folder + "/*.jpg")
#    print ("calculating descriptors for %d images" % len(files))
#    return concatenate([image_descriptors(file) for file in files])

def likelihood_moment(x, gaussians, weights, k, moment):
    x_moment = power(float32(x), moment) if moment > 0 else float32([1])
    probabilities = list(map(lambda j: weights[j] * gaussians[j], range(0, len(weights))))
#    print(probabilities)
    ytk = probabilities[k] / sum(probabilities)
    return x_moment * ytk

def likelihood_statistics(samples, means, covs, weights):
    s0, s1,s2 = {}, {}, {}

    n_samples = samples.shape[0]

    gaussians = {}
    try:
        g = [multivariate_normal(mean=means[k], cov=covs[k]) for k in range(0, len(weights)) ]
    except:
        print ('** Error ** singular matrix error in multivariate.  Allowing singular matrices...')
        g = [multivariate_normal(mean=means[k], cov=covs[k],allow_singular=True) for k in range(0, len(weights)) ]

    #print (g[0].pdf(means[0]))

    for i,x in zip(range(0, n_samples), samples):
        gaussians[i] = {k : g[k].pdf(x) for k in range(0, len(weights) ) }
        assert(not any(isnan(x)))

    for k in range(0, len(weights)):
        s0[k] = 0
        for j in range(n_samples):
            s0[k] += likelihood_moment(samples[j,:], gaussians[j], weights, k, 0)
        s1[k] = 0
        for j in range(n_samples):
            s1[k] += likelihood_moment(samples[j,:], gaussians[j], weights, k, 1)
        s2[k] = 0
        for j in range(n_samples):
            s2[k] += likelihood_moment(samples[j,:], gaussians[j], weights, k, 2)
        assert (not isnan(s0[k]))
        assert (not any(isnan(s1[k])))
        assert (not any(isnan(s2[k])))
 #       s0[k] = reduce(lambda a, (i,x): a + likelihood_moment(x, gaussians[i], weights, k, 0), samples, 0) # original author's code
 #       s1[k] = reduce(lambda a, (i,x): a + likelihood_moment(x, gaussians[i], weights, k, 1), samples, 0)
        #s2[k] = reduce(lambda a, (i,x): a + likelihood_moment(x, gaussians[i], weights, k, 2), samples, 0)
    return s0, s1, s2

def fisher_vector_weights(s0, s1, s2, means, covs, w, T):
    return float32([((s0[k] - T * w[k]) / sqrt(w[k]) ) for k in range(0, len(w))])

def fisher_vector_means(s0, s1, s2, means, sigma, w, T):
    return float32([(s1[k] - means[k] * s0[k]) / (sqrt(w[k] * sigma[k])) for k in range(0, len(w))])

def fisher_vector_sigma(s0, s1, s2, means, sigma, w, T):
    return float32([(s2[k] - 2 * means[k]*s1[k]  + (means[k]*means[k] - sigma[k]) * s0[k]) / (sqrt(2*w[k])*sigma[k])  for k in range(0, len(w))])

def normalize(fisher_vector):

    assert (not any(isnan(fisher_vector)))
    v = sqrt(abs(fisher_vector)) * sign(fisher_vector)
    return v / sqrt(dot(v, v))

def fisher_vector(samples, pca, means, covs, w):

    # apply pca transform
    #print ('Applying PCA transform %d x %d feature set' % (samples.shape[0], samples.shape[1]))
    samples = pca.transform(samples)
    #print (pca)
    #print ('Done')

    s0, s1, s2 =  likelihood_statistics(samples, means, covs, w)
    T = samples.shape[0]
    covs = float32([diagonal(covs[k]) for k in range(0, covs.shape[0])])
    a = fisher_vector_weights(s0, s1, s2, means, covs, w, T)
    b = fisher_vector_means(s0, s1, s2, means, covs, w, T)
    c = fisher_vector_sigma(s0, s1, s2, means, covs, w, T)
    assert (not any(isnan(a)))
    assert (not any(isnan(b)))
    assert (not any(isnan(c)))
    fv = concatenate([concatenate(a), concatenate(b), concatenate(c)])
    fv = normalize(fv)
    return fv

# parallel handler for feature computation
def compute_features (proc_num, image_list, im_r_size, return_dict):

    n_images = len(image_list)
    print('%d images to process (resized to %dx%d)' % (n_images, im_r_size, im_r_size))

    max_n_features_per_image = 200

    feature_size = 64
    out_vec = zeros((n_images*max_n_features_per_image,feature_size))
    total_n_features = 0

    for (img_name,count) in zip (image_list,range(1,n_images+1)):

        d = image_descriptors(img_name, im_r_size)

        n_features = d.shape[0]
        if total_n_features + n_features > out_vec.shape[0]:
            break

        out_vec[total_n_features:total_n_features+n_features,:] = d
        total_n_features += n_features

        # print progress bar
        if count % int(n_images/30) == 0:
            print ('[process %d] Progress %d%%' % (proc_num,100.0*count/n_images))

    out_vec = out_vec[:total_n_features,:]

    print ('return_dict[%d]=%dx%d' % (proc_num,out_vec.shape[0],out_vec.shape[1]))
    return_dict[proc_num] = out_vec

def generate_gmm(features_file_list, n_words, pca_feature_size, im_r_size):

    words = None

    t0 = time.clock()

    for file in features_file_list:
        print('\tloading %s' % file)
        v = pickle.load(open(file,'rb'))[1][0]
        if words is None:
            words = v
        else:
            words = vstack ((words,v))
    # stack features here...
#    words = concatenate (tuple([v for _,v in return_dict.items()]))

    print ('Features loading time : %.3f sec. (%d x %d features)' % (time.clock() - t0, words.shape[0], words.shape[1]))

    t0 = time.clock()
    print ("GMM - Training GMM of size (%d,%d).." % (len(words),n_words))
    pca, means, covs, weights = dictionary(words, n_words, pca_feature_size)
    print ('GMM Computing time : %.3f sec.' % (time.clock() - t0))

    #throw away gaussians with weights that are too small:
    th = 1.0 / n_words
    means = float32([m for k,m in zip(range(0, len(weights)), means) if weights[k] > th])
    covs = float32([m for k,m in zip(range(0, len(weights)), covs) if weights[k] > th])
    weights = float32([m for k,m in zip(range(0, len(weights)), weights) if weights[k] > th])

    print ('Saving GMM files to drive')
    gmm_folder = 'data'
    save(os.path.join(gmm_folder,"means.gmm"), means)
    save(os.path.join(gmm_folder,"covs.gmm"), covs)
    save(os.path.join(gmm_folder,"weights.gmm"), weights)
    pickle.dump(pca, open(os.path.join(gmm_folder,'pca.gmm'),'wb'))

    return pca, means, covs, weights

def get_fisher_vectors_from_image_list(image_list, gmm, im_r_size):
    #files = glob.glob(folder + "/*.jpg")
    print ("Computing Fisher vectors for %d images" % (len(image_list)))
    return float32([fisher_vector(image_descriptors(file, im_r_size), *gmm) for file in image_list])

#def fisher_features(image_list, gmm, im_r_size):
#    folders = glob.glob(folder + "/*")
#    print folders
#    features = {f : get_fisher_vectors_from_folder(f, gmm) for f in folders}
#    return features

def load_gmm(folder):
 #   files = ["means.gmm.npy", "covs.gmm.npy", "weights.gmm.npy"]
    return pickle.load(open(os.path.join('data/pca.gmm'),'rb')), load('data/means.gmm.npy'), load('data/covs.gmm.npy'), load('data/weights.gmm.npy')
#        list(map(lambda file: load(file), map(lambda s : folder + "/" , files)))

def fisher_compute_gmm (features_file_list, n_words, pca_feature_size, im_r_size):
    # generate GMM
    gmm_folder = 'data'
    gmm_file = 'data/means.gmm.npy'
    gmm = load_gmm(gmm_folder) if os.path.isfile(gmm_file) else generate_gmm(features_file_list, n_words, pca_feature_size, im_r_size)
    return gmm

def fisher_compute_vectors (gmm, image_list, n_words, im_r_size):

    # compute fisher vectors
    f_features = get_fisher_vectors_from_image_list (image_list, gmm, im_r_size)

    return f_features

#args = get_args()
#working_folder = args.file

#gmm = load_gmm(working_folder) if args.loadgmm else generate_gmm(working_folder, args.number)
#fisher_features = fisher_features(working_folder, gmm)

#TBD, split the features into training and validation
#classifier = train(working_folder, gmm, fisher_features)
#rate = success_rate(classifier, features)
#print ("Success rate is %s" % str(rate))

#def train(folder, gmm, features):
#    X = concatenate(features.values())
#    Y = concatenate([float32([i]*len(v)) for i,v in zip(range(0, len(features)), features.values())])

#    clf = svm.SVC()
#    clf.fit(X, Y)
#    return None# clf

#def success_rate(classifier, features):
#    print ("Applying the classifier...")
#    X = concatenate(features.values())
#    Y = concatenate([float32([i]*len(v)) for i,v in zip(range(0, len(features)), features.values())])
#    print (X.shape)
#    print (Y.shape)
#    res = float(sum([a==b for a,b in zip(classifier.predict(X), Y)])) / len(Y)


#def get_args():
#    parser = argparse.ArgumentParser()
#    parser.add_argument('-f' , "--file", help="Folder with images" , default='.')
#    parser.add_argument("-g" , "--loadgmm" , help="Load Gmm dictionary", action = 'store_true', default = False)
#    parser.add_argument('-n' , "--number", help="Number of words in dictionary" , default=5, type=int)
#    args = parser.parse_args()
#    return args

if __name__ == "__main__":

    # unit test GMM
    N = 2
    V = 64
    M = 100*V
    descriptors = concatenate((random.randn(M, V), 10+random.randn(M, V)))
    g = mixture.GMM(n_components=N, covariance_type='full')
    g.fit(descriptors)
    means = float32(g.means_)
    covs = float32(g.covars_)
    weights = float32(g.weights_)
    print (means)
    #print(covs)
    print(weights)

    g = [multivariate_normal(mean=means[k], cov=covs[k]) for k in range(0, len(weights)) ]

    print (g[0].pdf(means[0]))

