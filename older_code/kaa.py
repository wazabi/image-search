#!/usr/bin/env python
#
# This script fetches a catalog from a hive request file and saves images to files
#
#hive -e 'use catalog; select * from catalogs where partnerid=806 and reallyrecommendable;' > hive-output.txt
#hive -e 'use bi_data; select * from partnerdb_catalogs where partnerid=806 and reallyrecommendable;' > hive-output.txt
# 
#
import os
import re
import argparse
import random
import multiprocessing
from util import *
import numpy as np
import cv2
import urllib.request
import math
import glob

def download_url_array (url):

    req = urllib.request.Request(url,data=None,headers={\
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
    try:
        req = urllib.request.urlopen(req)
    except urllib.error.HTTPError as err:
        print('ERROR %d on URL %s' % (err.code,url))
        return None
    return np.asarray(bytearray(req.read()), dtype=np.uint8)


def fetch_images (inputfile, output_dir):

    #print ('Start process with input %d-%d'%(minid,maxid))
    #return

    fp = open (inputfile,'r',encoding="utf8")

    linecount = 0

    for line in fp.readlines():

        linecount = linecount + 1

        d = line.rstrip().split('\t')
        partnerid = int(d[13]) #d[0]
        #externalid = d[9] # d[1]
        internalid = int(d[20])#.zfill(5) #d[2]
        producturl1 = d[0] # d[16]
        producturl2 = d[19] # d[17]
        #category = d[1] # d[7]
            
        # build dirname
        if os.path.isdir (output_dir):
            shutil.rmtree (output_dir)
        os.makedirs (output_dir)

        outname = os.path.join (output_dir, '%s__%s.jpg' % (partnerid,internalid))

        # do not downnload same image twice
        if os.path.isfile (outname):
            #print( 'skipping %s' % outname)
            continue

        arr = None
        if producturl2 != 'NULL':
            arr = download_url_array (producturl2)
        if arr is None and producturl1 != 'NULL':
            arr = download_url_array (producturl1)
        if arr is None:
            continue

        im = cv2.imdecode(arr,-1)
        cv2.imwrite(outname,im)
        print ('%d %s' % (linecount, outname))
            
    fp.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--input', dest='input_files', type=str, required=True)
    parser.add_argument('--output', dest='output_dir', type=str, required=True)
    args = parser.parse_args()
    
    temp_dir = '/tmp/'

    for input_file in glob.iglob(args.input_files):

        nlines = util_count_lines (input_file)
        sampling_ratio = 1#min([1.0,1.0 * max_images / nlines])

        # split input file into N files
        print ('Splitting input file %s into %d files (sampling ratio = %.3f)...' % (input_file, n_processes, sampling_ratio))
        temp_files = util_split_file (input_file, n_processes, sampling_ratio)
        print ('done.')

        jobs = []

        for n in range(n_processes):
            process = multiprocessing.Process (target=fetch_images, args=[temp_files[n],temp_dir])
            process.start()
            jobs.append(process)

        # wait for everyone
        for proc in jobs:
            proc.join()

        # remove temp files
        for temp_file in temp_files:
            os.remove(temp_file)

        print ('Dowload done for input file %s' % input_file )


    # scp files to remote dir
    host = 'ubuntu@176.34.228.64'
    cmd = 'scp %s/* *%s:%s' % (temp_dir, host, args.output_dir)
    print (cmd)
    with open(os.devnull,'w') as fnull:
        subprocess.call (cmd, shell=True, stdout=fnull, stderr=fnull)

