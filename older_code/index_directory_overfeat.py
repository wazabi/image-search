import os
import sys
import subprocess
import progressbar as pb
import argparse
import json
import numpy as np
import pickle
from util import util_feature_filename, util_get_partnerid_from_file
import ConfigParser
import math
import glob
import shutil
import random
import multiprocessing
sys.path.insert(0,'/home/ubuntu/caffe/python')
import caffe
import time
import matplotlib 
matplotlib.use('Agg') 

def count_files(dir):
    result = 0
    for root, dirs, files in os.walk(dir):
        result += len(files)
    return result

def resize2_images(input_directory, resized_directory):
    #resize images
    target_size=231
    nb_files = count_files(input_directory)
    print('Resizing %d images...' % nb_files)
    #widgets = ['Resizing: ', pb.Counter(), ", ", pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
    #pbar = pb.ProgressBar(widgets=widgets, maxval=count_files(input_directory)).start()
    for root, dirs, files in os.walk(input_directory):
        for file in files:
            if not os.path.basename(file).endswith('.jpg'):
                continue
            dest = os.path.join(resized_directory, file[0], file[1], file[2], file)
            if not os.path.exists(os.path.dirname(dest)):
                os.makedirs(os.path.dirname(dest))
            #pbar.update(pbar.currval + 1)
            command = "convert %s -resize %dx%d^ -gravity Center -crop %dx%s+0+0 +repage %s" % (os.path.join(root, file), target_size, target_size, target_size, target_size, dest)
            subprocess.Popen([command], shell=True, stdout=subprocess.PIPE).stdout.readlines()
    #pbar.finish()

def resize_images (img_files, output_directory):
    
    target_size = 231

    for (a,b) in img_files:
        dest = os.path.join(output_directory, a)
        command = "convert %s -resize %dx%d^ -gravity Center -crop %dx%s+0+0 +repage %s" % (b, target_size, target_size, target_size, target_size, dest)
        subprocess.Popen([command], shell=True, stdout=subprocess.PIPE).stdout.readlines()

def caffe_build_net ():
    caffe_root = '/home/ubuntu/caffe/'
    if not os.path.isfile(caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel'):
            print("You need to download the caffe model first...")
            print("Type : ./scripts/download_model_binary.py ../models/bvlc_reference_caffenet in CAFFE_ROOT")
            return
    
    #caffe.set_mode_cpu()
    # GPU mode
    caffe.set_mode_cpu()
    
    nets = {}
    net = caffe.Net(caffe_root + 'models/bvlc_reference_caffenet/deploy.prototxt',
                    caffe_root + 'models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel',
                    caffe.TEST)

    # input preprocessing: 'data' is the name of the input blob == net.inputs[0]
    transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
    transformer.set_transpose('data', (2,0,1))
    transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
    transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
    transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

    print('Initialization done')

    return (net,transformer)


def compute_features_caffe (proc_id, net, input_directory, feature_directory):

    caffe_root = '/home/ubuntu/caffe/'

    print('entering compute...')
    (net, transformer) = net

    # run one pass forward
    #net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image(caffe_root + 'examples/images/cat.jpg'))
    for b in range(100000):

        print('init the data...')
        # set net to batch size of 50
        net.blobs['data'].reshape(50,3,227,227)
        print('setting the data...')
        print( transformer.preprocess('data', caffe.io.load_image(caffe_root + 'examples/images/cat.jpg')))
        print('setting the data really...')
        net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image(caffe_root + 'examples/images/cat.jpg'))

        print('Running forward...')
        t0 = time.clock()
        out = net.forward()
        elapsed_time = time.clock() - t0
        print('Elapsed time: %.6f sec.' % elapsed_time)

        print("Predicted class is #{}.".format(out['prob'][0].argmax()))

        print([(k, v.data.shape) for k, v in net.blobs.items()])

        feat = [x for x in net.blobs['fc7'].data[0].flat]
    #    print((feat))

def compute_features_overfeat (input_directory, feature_directory, overfeat_lib, overfeat_batch):

    feature_extraction_cmd="LD_LIBRARY_PATH=%s %s -i %s -o %s" % (overfeat_lib, overfeat_batch, input_directory, feature_directory)
    with open(os.devnull, "w") as fnull:
        print(feature_extraction_cmd)
        subprocess.call(feature_extraction_cmd, shell=True, stdout=fnull, stderr=fnull)


def load_features(input_directory, feature_directory, data_dir, feature_file_basename, n_chunks):
    file_list = []
    for root, dirs, files in os.walk(feature_directory):
        for file in files:
            if not os.path.basename(file).endswith('.jpg.features'):
                continue
            file_list.append(os.path.join(root, file))
    
    # shuffle the files to mix categories in the chunks (filenames start by product category)
    random.seed(123456)
    random.shuffle(file_list)

    block_size = int(math.floor(len(file_list)/n_chunks))

    nb_files = len(file_list)
    print ('# files : %d.  block size:%d ' % (nb_files,block_size))
    widgets = ['Loading Features: ', pb.Counter(), ", ", pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
    pbar = pb.ProgressBar(widgets=widgets, maxval=nb_files).start()

    for i, chunk in enumerate(chunks(file_list, block_size)):
        features = []
        paths = []
        chunk = list(chunk)
        for path in chunk:
            feature = open(path).readlines()[1].strip().split(" ")
            feature = [float(f) for f in feature]
            features.append(feature)
            filename = os.path.relpath(path, feature_directory).replace('.features', '')
            paths.append(input_directory+'/'+filename)
            pbar.update(pbar.currval + 1)
        features = np.array(features)
        paths = np.array(paths)
        paths.reshape(paths.shape[0], 1)
        normed_matrix = features / np.linalg.norm(features, axis=-1)[:, np.newaxis]
        feature_filename = util_feature_filename(data_dir, feature_file_basename, partnerid, 'cnn', 0, i)
        # pickle with highest protocol to ensure windows/linux portability (default protocol 0 is not)
        pickle.dump((paths,(normed_matrix,[normed_matrix.shape[1],0])),open(feature_filename,'wb'),protocol=pickle.HIGHEST_PROTOCOL)

    #    output_file = os.path.join(output_dir, "%04d.vectors.npy" % i)
    #    np.save(output_file, normed_matrix)
    #    output_file = os.path.join(output_dir, "%04d.names.npy" % i)
    #    np.save(output_file, paths)
    pbar.finish()

def resize_and_compute (proc_id, img_files, output_dir, nets, overfeat_lib, overfeat_batch):

    resized_directory = '/tmp/img_resized_%d' % proc_id
    feature_directory = '/tmp/features_%d' % proc_id

    # resize images
    if os.path.isdir(resized_directory):
        shutil.rmtree(resized_directory)
    os.makedirs(resized_directory)

    resize_images (img_files, resized_directory)

    # compute features
    if os.path.isdir(feature_directory):
        shutil.rmtree(feature_directory)
    os.makedirs(feature_directory)

#    compute_features_overfeat (resized_directory, output_directory, overfeat_lib, overfeat_batch)
    compute_features_caffe (proc_id, nets, resized_directory, output_dir)

if __name__ == "__main__":

    # read config file
    config = ConfigParser.ConfigParser()
    config.readfp(open('defaults.ini'))
    overfeat_batch = config.get('Overfeat','overfeat_batch')
    overfeat_lib = config.get('Overfeat','overfeat_lib')
    input_dir = config.get('Overfeat','input_dir')
    output_dir = config.get('Overfeat','output_dir')
    feature_file_basename = config.get('Features','feature_file_basename')

    # list files
    img_files = []
    for root, dirs, files in os.walk(input_dir):
        for filename in files:
            _, ext = os.path.splitext(filename)
            if ext == '.jpg':
                img_files.append((filename,os.path.join(root,filename)))

    n_proc = max([32,int(len(img_files)/1000)])
   
    n_proc = 1

    print('# proc = %d, # images = %d' % (n_proc, len(img_files)))

    # distribute in round-robin
    img_files_gp = {}
    for k in range(n_proc):
        img_files_gp[k] = []
    for ((a,b),c) in zip(img_files,range(len(img_files))):
        img_files_gp[c%n_proc].append((a,b))

    # caffe build nets
    nets = caffe_build_net ()

    jobs = []
    
    for k in range(n_proc):

        process = multiprocessing.Process (target=resize_and_compute, args=[k, img_files_gp[k], output_dir, nets, overfeat_lib, overfeat_batch])
        process.start()

    for proc in jobs:
        proc.join()
            
