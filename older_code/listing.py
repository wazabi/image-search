import os
import glob
import configparser
from util import util_get_partnerid_from_file

def list_files (data_dir, partnerid, output_file, file_ext):

    n_files = 0
    fp = open (output_file, 'w')

    for filename in glob.iglob('%s/img/%d/**/%s' % (data_dir, partnerid, file_ext), recursive=True):
        fp.write('%s\n' % filename)
        n_files += 1

    fp.close()
    return n_files

if __name__ == "__main__":

    # read config file
    config = configparser.ConfigParser()
    config.readfp(open('defaults.ini'))

    partnerid = util_get_partnerid_from_file(config.get ('Catalog','input_file'))
    data_dir = config.get ('Catalog','data_dir')
    file_ext = config.get('Listing','file_ext')

    print('Partner ID : %d' % partnerid)

    output_file = os.path.join (data_dir, 'imlist-%d.txt' % partnerid)

    n_files = list_files (data_dir, partnerid, output_file, file_ext)

    print ('%d images' % n_files)
