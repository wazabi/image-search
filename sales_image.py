#!/usr/bin/env python
import os
import img_quality
import numpy as np
import math
import warnings
import multiprocessing
import pickle
from random import shuffle
from scipy import stats 

def stats_dist_differ (u,v):
    # Mann-Whitney-Wilcoxon (MWW) RankSum test
    z_stat, p_val_mww = stats.ranksums(u,v)
    # Welch test
    t_stat, p_val_w = stats.ttest_ind(u,v,equal_var=False)

    return (p_val_mww, p_val_w)


def image_filename (partner_id, external_item_id):
    return os.path.join ('/opt/_img/','%d__%d.jpg' % (partner_id, external_item_id))


def stack_set (data):
    output = np.array([])
    for k,v in data.iteritems():
        output = np.append(output,v)
    return output


def read_sales_file (filename):
    data = []
    with open(filename,'r') as fp:
        lines = fp.readlines()
        for line in lines:
            d = line.strip().split(',')
            partner_id = int(d[0])
            external_item_id = int(d[1])
            data.append((partner_id, external_item_id))
    return data


def score_set_images (proc,data,datafile):
    scores_per_partner = {}
    progress = 0
    n_scores = 0
    for (s,dcount) in zip(data,range(len(data))):
        partner_id = s[0]
        external_item_id = s[1]
        img_name = image_filename (partner_id, external_item_id)
        if not os.path.isfile(img_name):
            continue
        if os.path.isfile (img_name):
            filename = os.path.basename (img_name)
            dirname = os.path.dirname (img_name)
            imq = img_quality.ImageQuality (dirname, filename)
            imq.compute_params()
            imq.compute_score()
            if imq.score_valid:
                score = imq.score
                n_scores = len(score)
                if partner_id not in scores_per_partner:
                    scores_per_partner[partner_id] = [ np.array([]) for k in range(n_scores)]
                for k in range(n_scores):
                    if (not np.isnan (score[k])) and (not math.isnan (score[k])):
                        scores_per_partner[partner_id][k] = np.append (scores_per_partner[partner_id][k], score[k])
        current_progress = int(100.0*dcount/len(data))
        if current_progress > progress:
            progress = current_progress
            if proc == 0:
                print('%d %%' % progress)
   
    # filter out nan values
    #for k,v in scores_per_partner.iteritems():
    #    for i in range(n_scores):
    #        scores_per_partner[k][i] = scores_per_partner[k][i][~np.isnan(scores_per_partner[k][i])]

    pickle.dump (scores_per_partner, open(datafile,'wb'))



def compute_stats (scores1, scores2):
   
    print ('computing stats...')

    full_set_1 = {}
    full_set_2 = {}
    output_file = {}

    n_scores = 0
    for k,v in scores1.iteritems():
        if not k in scores2:
            continue
        sc1 = v
        sc2 = scores2[k]
        n_scores = len(sc1)
        assert (len(sc2)==n_scores)

        for i in range (n_scores):
        
            if i not in output_file:
                output_file[i] = open ('stats-%d.txt' % i, 'w')

            vsc1 = sc1[i]
            vsc2 = sc2[i]
            if (np.size(vsc1)==0) or (np.size(vsc2)==0):
                continue

            # update full sets for global stats
            if i not in full_set_1:
                full_set_1[i] = np.array([])
            if i not in full_set_2:
                full_set_2[i] = np.array([])
            full_set_1[i] = np.append (full_set_1[i], vsc1)
            full_set_2[i] = np.append (full_set_2[i], vsc2)

            # compute stats per partner
            (p_val_mww, p_val_w) = stats_dist_differ (vsc1,vsc2)
            mean1 = np.mean(vsc1)
            mean2 = np.mean(vsc2)
            std1 = np.std(vsc1)
            std2 = np.std(vsc2)
            count1 = np.size(vsc1)
            count2 = np.size(vsc2)
            print ('Partner %20d\t Score %d\t[%d] %.6f [%.6f]\t[%d] %.6f [%.6f]\tp_val = %.5f,%.5f' % (k,i,count1,mean1,std1,count2,mean2,std2,p_val_mww,p_val_w))
            output_file[i].write ('%d,%d,%.5f,%.5f,%d,%.5f,%.5f,%d,%.5f,%.5f\n' % (k,count1,mean1,std1,count2,mean2,std2,mean1>mean2,p_val_mww,p_val_w))

    # compute global stats
    for i in range (n_scores):
        (p_val_mww, p_val_w) = stats_dist_differ (full_set_1[i], full_set_2[i])
        mean1 = np.mean(full_set_1[i])
        mean2 = np.mean(full_set_2[i])
        std1 = np.std(full_set_1[i])
        std2 = np.std(full_set_2[i])
        count1 = np.size(full_set_1[i])
        count2 = np.size(full_set_2[i])
        print ('Global\t\t\t\t [%d] %.6f [%.6f]\t[%d] %.6f [%.6f]\tp_val = %.5f,%.5f' % (count1,mean1,std1,count2,mean2,std2,p_val_mww,p_val_w))
        output_file[i].write ('0,%d,%.5f,%.5f,%d,%.5f,%.5f,%d,%.5f,%.5f\n' % (count1,mean1,std1,count2,mean2,std2,mean1>mean2,p_val_mww,p_val_w))
        output_file[i].close()


def fuse_scores (file_list):
    output = {}
    n_scores = 0
    for filename in file_list:
        data = pickle.load (open(filename,'rb'))
        for k,v in data.iteritems():
            n_scores = len(v)
            if k in output:
                for i in range(n_scores):
                    output[k][i] = np.append (output[k][i],v[i])
            else:
                output[k] = [np.array([]) for i in range(n_scores)]
                for i in range(n_scores):
                    output[k][i] = np.copy (v[i])
    return output


def parallel_process (data):

    n_proc = 32

    # split data
    print ('splitting data...')
    data_k = [ [] for k in range(n_proc)]
    data_files = [ '/tmp/temp-salesimages-%d.bin' % k for k in range(n_proc)]
    for k in range(len(data)):
        data_k[k%n_proc].append (data[k])

    # launch parallel computation
    print ('launch jobs...')
    jobs = []
    for proc in range(n_proc):
        process = multiprocessing.Process (target=score_set_images, args=[proc, data_k[proc], data_files[proc]])
        process.start()
        jobs.append(process)

    # wait for all
    for job in jobs:
        job.join()

    # fuse output
    scores = fuse_scores (data_files)

    return scores


def main():
    sales_file = '/opt/sales.txt'
    nosales_file = '/opt/nosales.txt'
    img_dir = '/opt/_img'
    maxn = -1
    shuffle_it = False

    # do not shuffle very small samples
    if maxn > 0 and maxn < 2000:
        shuffle_it = False

    # read sales 
    print('reading data...')
    sales = read_sales_file (sales_file)
    print('done')

    if shuffle_it:
        shuffle (sales)
    if maxn > 0:
        sales = sales[:maxn]

    # read nosales
    print('reading data...')
    nosales = read_sales_file (nosales_file)
    print('done')

    if shuffle_it:
        shuffle (nosales)
    if maxn > 0:
        nosales = nosales[:maxn]

    # compute image scores
    scores_sales = parallel_process (sales)
    scores_nosales = parallel_process (nosales)
  
    # compute stats
    compute_stats (scores_sales, scores_nosales)


if __name__ == "__main__":
#    warnings.simplefilter('error', UserWarning)
    main()
