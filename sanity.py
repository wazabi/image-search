#!/usr/bin/env python
import os
import time
import json
import datetime
from util import util_file_size, util_run_cmd, util_send_email
import subprocess
from pid import PidFile

def _log (msg):
    dt_string = datetime.datetime.now().strftime ('%Y-%m-%d %H:%M:%S')
    filename = os.path.basename(__file__)  
    msg_string = '%s\t[%s]\t%s' % (dt_string, filename, msg)
   
    print (msg_string)

    with open('/home/o.koch/image-search/_%s.log' % filename,'a') as fp:
        fp.write ('%s\n' % msg_string)
        fp.close()

    # log error messages (but ignore HDFS warnings)
    if msg.find ('ERROR') is not -1 and msg.find ('WARN retry.RetryInvocationHandler') == -1:
        with open('/home/o.koch/image-search/_%s.log.error' % os.path.basename(__file__) ,'a') as fp:
            fp.write ('%s\n' % msg_string)
            fp.close()


def delay_sec_to_string (d):
    if d < 60:
        return '%d secs' % d
    if d < 3600:
        return '%d mins' % round(1.0*d/60)
    return '%1.1f hours' % (1.0 * d / 3600)


def exists_dir_hdfs(path):
    proc = subprocess.Popen(['hadoop','fs','-test','-d',path])
    proc.wait()
    return proc.returncode == 0

def exists_file_hdfs(path):
    proc = subprocess.Popen(['hadoop','fs','-test','-e',path])
    proc.wait()
    return proc.returncode == 0

def send_jar_command (channel, metric, value):
    jar_cmd = 'yarn jar /home/o.koch/ref/recocomputer/releases/1455705605_1.0-SNAPSHOT/lib/criteo-hadoop-recocomputer.jar com.criteo.hadoop.recocomputer.utils.logging.GraphiteMetricsLogger'
    cmd = '%s %s %s:%s' % (jar_cmd, channel, metric, value)
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)


def check_partners ():

    hdfs_input_json_filename = '/user/o.koch/cnn/outputByPartner'
    input_json_filename = '/home/o.koch/image-search/.input.outputByPartner.sanity'
    hdfs_root = '/user/o.koch/cnn/'
   
    # remove local file
    if os.path.isfile (input_json_filename):
        os.remove (input_json_filename)

    # fetch output outputByPartner on HDFS
    cmd = 'hadoop fs -get %s %s' % (hdfs_input_json_filename, input_json_filename)
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return

    # parse input data
    input_data = None
    with open(input_json_filename,'r') as fp:
        json_data = fp.read()
        try:
            input_data = json.loads(json_data)
        except:
            _log('*** ERROR *** Failed to read JSON file %s.  Exiting.' % input_json_filename)
            return

    # check that all files exist on HDFS
    n_files=0
    n_files_success=0

    for item in input_data:

        # check that files exist on HDFS
        output_folder = input_data[item]['outputFolder']
        output_files = input_data[item]['files']
        for filename in output_files:
            hdfs_path = os.path.join (hdfs_root, output_folder, filename)
            n_files += 1
            _log ('checking %s' % hdfs_path)
            if not exists_file_hdfs (hdfs_path):
                _log('*** ERROR *** File %s does not exist on HDFS but is listed in outputByPartner.' % hdfs_path)
            else:
                n_files_success += 1

    _log('%d/%d files checked successfully on HDFS.' % (n_files_success, n_files))


if __name__ == "__main__":

    with PidFile():
        check_partners()
