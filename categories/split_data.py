import os
import glob
import random
import util
import argparse


def shuffle_files(input_dir, output_dir):
    """ Split data into new files with shuffling
    """
    util.reset_dir (output_dir)

    n_files = 1000
    ops = [open(os.path.join(output_dir,'part-%05d'%i),'w') for i in range(n_files)]

    for filename in glob.glob(input_dir+'/part*'):
        print(filename)
        with open (filename,'r') as fp:
            for line in fp.readlines():
                ind = random.randint(0,n_files-1)
                ops[ind].write(line)

    for op in ops:
        op.close()


def split_files (input_dir, output_dir1, output_dir2, ratio):
    """ Split files between two sets
    """
    util.reset_dir (output_dir1)
    util.reset_dir (output_dir2)

    filelist = list(glob.glob(os.path.join(input_dir,'part-*')))
    random.shuffle (filelist)

    ns = int(len(filelist)*ratio)
    list1 = filelist[:ns]
    list2 = filelist[ns:]

    for filename in list1:
        cmd = 'mv %s %s' % (filename, output_dir1)
        print (cmd)
        os.system (cmd)

    for filename in list2:
        cmd = 'mv %s %s' % (filename, output_dir2)
        print (cmd)
        os.system (cmd)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    args = parser.parse_args()

    shuffled_dir = args.input_dir+'Shuffled'
    outdir_train = shuffled_dir+'Train'
    outdir_test = shuffled_dir+'Test'

    print ("shuffling data...")
    shuffle_files(args.input_dir, shuffled_dir)

    print ("splitting data...")
    ratio = 0.8
    split_files (shuffled_dir, outdir_train, outdir_test, ratio)



if __name__ == "__main__":
    main()


