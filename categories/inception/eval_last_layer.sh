# Directory where we saved the fine-tuned checkpoint and events files.
TRAIN_DIR=/ops/tmp

# Directory where the flowers data resides.
DATA_DIR=/ops/tf-data/small

# Directory where to save the evaluation events files.
EVAL_DIR=/ops/tmp3

bazel-bin/inception/flowers_eval --eval_dir="${EVAL_DIR}" --data_dir="${DATA_DIR}" --subset=validation --num_examples=500 --checkpoint_dir="${TRAIN_DIR}" --input_queue_memory_factor=1 --run_once
