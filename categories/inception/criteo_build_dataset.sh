#!/bin/bash

bazel build inception/build_image_data

stdbuf -o0 bazel-bin/inception/build_image_data \
      --train_directory=/ops/lsid/large \
        --validation_directory=/ops/criteo-data/images \
          --output_directory=/ops/tf-data/criteo \
            --labels_file=/ops/image-search/categories/lsid/criteo_categories.txt \
              --train_shards=128 \
                --validation_shards=24 \
                  --num_threads=8

