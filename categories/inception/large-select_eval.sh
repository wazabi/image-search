# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/imagenet_eval

CHECKPOINT_DIR=/ops/tmp/large-select-train
OUTPUT_DIR=/ops/tmp/large-select-eval
DATA_DIR=/ops/tf-data/large-select

bazel-bin/inception/imagenet_eval --checkpoint_dir=${CHECKPOINT_DIR} --eval_dir=${OUTPUT_DIR} --data_dir=${DATA_DIR} --num_examples=77315


