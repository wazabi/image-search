bazel-bin/inception/build_image_data \
      --train_directory=/ops/lsid/full_train \
        --validation_directory=/ops/lsid/full_test \
          --output_directory=/ops/tf-data/full \
            --labels_file=/home/ubuntu/image-search/categories/lsid/full_categories.txt \
              --train_shards=128 \
                --validation_shards=24 \
                  --num_threads=8

