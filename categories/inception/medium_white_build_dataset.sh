stdbuf -o0 bazel-bin/inception/build_image_data \
      --train_directory=/ops/lsid/medium_white_train \
        --validation_directory=/ops/lsid/medium_white_test \
          --output_directory=/ops/tf-data/medium_white \
            --labels_file=/home/ubuntu/image-search/categories/lsid/medium_categories.txt \
              --train_shards=128 \
                --validation_shards=24 \
                  --num_threads=8

