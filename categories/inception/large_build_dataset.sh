#!/bin/bash

bazel build inception/build_image_data

stdbuf -o0 bazel-bin/inception/build_image_data \
      --train_directory=/ops/lsid/large-train \
        --validation_directory=/ops/lsid/large-test \
          --output_directory=/ops/tf-data/large \
            --labels_file=/ops/image-search/categories/lsid/large_categories.txt \
              --train_shards=128 \
                --validation_shards=24 \
                  --num_threads=8

