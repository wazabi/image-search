# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/imagenet_train

# run it
stdbuf -o0 bazel-bin/inception/imagenet_train --num_gpus=8 --batch_size=64 --train_dir=/ops/tmp/medium_train --data_dir=/ops/tf-data/medium

