# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/imagenet_eval

CHECKPOINT_DIR=/ops/tmp/criteo_train
OUTPUT_DIR=/ops/tmp/criteo_eval
DATA_DIR=/ops/tf-data/criteo

stdbuf -o0 bazel-bin/inception/imagenet_eval --checkpoint_dir=${CHECKPOINT_DIR} --eval_dir=${OUTPUT_DIR} --data_dir=${DATA_DIR} --num_examples=48138


