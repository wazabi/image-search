# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/imagenet_eval

# run it
bazel-bin/inception/imagenet_eval --checkpoint_dir=/ops/inception-v3-model/inception-v3 --eval_dir=/ops/tmp/imagenet_eval --data_dir=/opt/imagenet-data
#bazel-bin/inception/imagenet_eval --checkpoint_dir=/ops/inception-v3-model/inception-v3 --eval_dir=/ops/tmp/imagenet_eval --data_dir=/opt/imagenet-data --subset=train --num_examples=1281167
