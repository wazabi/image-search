import os
import glob
import argparse
import multiprocessing
import requests
import shutil
import re
import util
import time
import random
import json
from random import shuffle
import subprocess as sp
import datetime
import mmh3
import urllib

def error_log (msg):
    timestamp_str = datetime.datetime.now().strftime("%B %d, %Y -- %I:%M%p")
    print ('%s : %s' % (timestamp_str,msg))
    with open ('err','a') as gp:
        gp.write ('%s : %s\n' % (timestamp_str,msg))

def log (mstr):
    timestr = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print ('%s -- %s' % (timestr, mstr))            


def recursiveUrlDecode(externalIdFromImageDl):
    decodedUrl = urllib.unquote(externalIdFromImageDl)
    if decodedUrl != externalIdFromImageDl:
        return recursiveUrlDecode(decodedUrl)
    return decodedUrl

def hash_external (external_item_id):
    hashed_external = mmh3.hash64 (recursiveUrlDecode(external_item_id.lower()), 42, True)
    hashed_external = hashed_external[0] ^ hashed_external[1]
    return hashed_external


def parse_json_file (json_filename):
    """ parse a json file to extract URL, partnerid, externalId 
    """
    out = []
    try:
        # try tro extract categoryId from filename
        categoryid = int(os.path.splitext(os.path.basename(json_filename))[0])
    except:
        categoryid = -1
    with open (json_filename,'r') as fp:
        lines = fp.readlines()
        for line in lines:
            d = json.loads(line)
            partnerid = int(d['partnerId'])
            if 'externalId' in d and 'hashedExternalId' in d:
                #print ("%s --> %s" % (hash_external(d['externalId'].encode('utf-8')), d['hashedExternalId']))
                assert (hash_external(d['externalId'].encode('utf-8'))==d['hashedExternalId'])
            try:
                externalid = int(d['hashedExternalId'])
            except:
                try:
                    externalid = hash_external(d['externalId'])
                except:
                    print ('ERROR: failed to find hashedExternalId or externalId fields in following line:')
                    print (line)
                    continue
            if 'googleProductCategoryId' in d:
                categoryid = int(d['googleProductCategoryId'])
            try:
                url = d['bigimage']
            except:
                try:
                    url = d['smallimage']
                except:
                    continue
            if categoryid != -1:
                out.append((partnerid,externalid,categoryid,url))
    return out


def list_files (dirname):
    """ list json files in the directory
    """
    out = []
    limit=-1
    for root, dirs, files in os.walk (dirname):
        for name in files:
            if name.find('.json') is not -1:
                out.append (os.path.join(root,name))
            if limit>0 and len(out)>limit:
                break
        if limit>0 and len(out)>limit:
            break
    return out


def build_job_list (filelist, n_proc):
    """ Build a list of URLs to download
    """
    print ('%d files' % len(filelist))
    tasklist = []
    for (filename, count) in zip(filelist, range(len(filelist))):
        print (count)
        tasklist += parse_json_file (filename)
    shuffle (tasklist)

    print ('%d tasks.' % len(tasklist))

    joblist = [[] for j in range(n_proc)]
    for (task,k) in zip(tasklist,range(len(tasklist))):
        joblist[k%n_proc].append(task)

    print ('%d tasks after split' % (sum([len(a) for a in joblist])))

    return joblist


def reset_dir (dirname):
    """ Erase and re-create a directory
    """
    if os.path.isdir (dirname):
        shutil.rmtree (dirname)
    os.makedirs (dirname)

def download_image_2 (url, procid, filename, dry_run_flag):
    """ download an image from the web using requests
    """
    if dry_run_flag:
        print ('[dry run] downloading %s --> %s'% (url, filename))
        return True

    success=False
    tmp_dir='/tmp/down-%d' % procid
    reset_dir (tmp_dir)
    FNULL = open(os.devnull,'w')
    ERRLOG = open('/tmp/err','w')
    #proc = sp.Popen(['wget','--no-check-certificate','-U','Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0','-P',tmp_dir,url], stdout=FNULL, stderr=FNULL)
    proc = sp.Popen(['wget','--no-check-certificate','-T','2','-U','firefox','-P',tmp_dir,'--tries','8',url], stdout=FNULL, stderr=FNULL)
    proc.wait()
    if proc.returncode == 0:
        wfilename = filter (lambda a : os.path.isfile(os.path.join(tmp_dir,a)), os.listdir(tmp_dir))
        if len(wfilename)==1:
            wfilename = wfilename[0]
            wfullname = os.path.join (tmp_dir, wfilename)
            ext = os.path.splitext (wfilename)[1]
            if ext == '.jpeg' or ext == '.jpg':
                shutil.move(wfullname, filename)
                success=True
            else:
                proc = sp.Popen(['convert',wfullname,filename], stdout=ERRLOG, stderr=ERRLOG)
                proc.wait()
                success = (proc.returncode==0)
                if not success:
                    error_log ('ERROR running convert command %s --> %s. Return code : %d' % (wfullname, filename, proc.returncode))
        else:
            error_log ('ERROR : no single file in %s after wget on %s' % (tmp_dir, url))
    else:
        error_log('ERROR : wget failed on URL %s (return code : %d)' % (url, proc.returncode))
    return success


def download_image (url, filename):
    """ download an image from the web using requests
    """
    n_attempts=10
    success = False
    for k in range(n_attempts):
        try:
            response = requests.get(url, stream=True)
        except:
            time.sleep(.2)
            continue
        if response.status_code == 200:
            with open(filename, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
                success = True
        del response
        if success:
            break
    return success


def parallel_run (output_dir, proc_id, job_list, dry_run_flag):
    """ download images
    """
    for j in job_list:
        partnerid = j[0]
        externalId = j[1]
        category = j[2]
        url = j[3]
        local_outdir = os.path.join (output_dir, '%d/' % category)
        if not os.path.isdir (local_outdir) and not dry_run_flag:
            os.makedirs (local_outdir)
        out_filename = os.path.join (local_outdir, '%d_%d.jpg'%(partnerid, externalId))
        # do not download same image twice
        if os.path.isfile (out_filename):
            continue
        if dry_run_flag:
            print ('[dry run] %s --> %s...' % (url, out_filename))
        else:
            print ('%s --> %s...' % (url, out_filename))
        if not download_image_2 (url, proc_id, out_filename, dry_run_flag):
            print ('ERROR : failed to download image from URL %s' % url)


def execute_list (job_lists, output_dir, dry_run_flag):
    """ Execute a job list
    """
    n_proc = len(job_lists)
    jobs = []
    for proc in range(n_proc):
        process = multiprocessing.Process (target=parallel_run, args=[output_dir, proc, job_lists[proc], dry_run_flag])
        process.start()
        jobs.append(process)

    # wait for all
    for job in jobs:
        job.join()


def count_images (indir):
    filelist = glob.glob(indir+'/*/*.jpeg')
    return (len (filelist))


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory', required=True)
    parser.add_argument('--nproc', dest='n_procs', type=int, help='number of processes to launch', required=False, default=32)
    parser.add_argument('--dry', dest='dry_run', action='store_true', default=False)
    args = parser.parse_args()

    filelist = list_files (args.input_dir)

    # build job list
    job_lists = build_job_list (filelist, args.n_procs)

    # clear output
#    print ('cleaning output...')
#    util.reset_dir (args.output_dir)

    # execute
    execute_list (job_lists, args.output_dir, args.dry_run)

    # count images
    #n_images = count_images (args.output_dir)
    #print ('output dir has %d images.' % n_images)


if __name__ == "__main__":
    main()
