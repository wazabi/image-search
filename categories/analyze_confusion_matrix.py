import argparse
import glob
import os
import numpy as np

def accuracy (M):
    return 1.0 * np.sum(np.diagonal(M)) / np.sum(M)


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--in', dest='input_file', type=str, help='input file containing matrix in npy format', required=True)
    args = parser.parse_args()

    M = np.load (args.input_file)
    print (np.shape(M))
    print (np.sum(M))
    print (accuracy (M))

if __name__ == "__main__":

    main()
