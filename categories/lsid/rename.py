import glob
import shutil

files = glob.glob ('/ops/lsid/list/*/*.done')
for filename in files:
    print (filename)
    new_name = filename.replace('.done','') + '.success'
    print (new_name)
    shutil.move (filename, new_name)
