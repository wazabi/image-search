import re, os, sys, datetime, time
import pandas
import urllib2
import json
from selenium import webdriver
from contextlib import closing
from selenium.webdriver import Firefox
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from pattern.web import URL, extension, cache, plaintext, Newsfeed, DOM

class GoogleImageExtractor(object):

    def __init__(self):
        """ Google image search class
            Args:
                search_key to be entered.

        """

        #self.g_search_key = ''
        #self.g_search_id = search_id

        ## user options
        self.image_dl_per_search = 2000

        ## url construct string text
        self.prefix_of_search_url = 'https://yandex.com/images/search?text='
        self.postfix_of_search_url = ''

        self.target_url_str = ''

        ## storage
        self.pic_url_list = []
        self.pic_info_list = []

        binary = FirefoxBinary(r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe')
        self.driver = webdriver.Firefox(firefox_binary=binary)  # webdriver.Firefox()
        self.driver.maximize_window()

        ## file and folder path
        self.folder_main_dir_prefix = r'images'

    def setCategory (self,search_key):
        self.g_search_key = search_key
        if type(search_key) == str:
            ## convert to list even for one search keyword to standalize the pulling.
            self.g_search_key_list = [search_key]
        elif type(search_key) == list:
            self.g_search_key_list = search_key
        else:
            print ('google_search_keyword not of type str or list')
            raise

    def setCategoryId (self,xid):
        self.g_search_id = xid

    def reformat_search_for_spaces(self):
        """
            Method call immediately at the initialization stages
            get rid of the spaces and replace by the "+"
            Use in search term. Eg: "Cookie fast" to "Cookie+fast"

            steps:
            strip any lagging spaces if present
            replace the self.g_search_key
        """
        self.g_search_key = self.g_search_key.rstrip().replace(' ', '+')

    def set_num_image_to_dl(self, num_image):
        """ Set the number of image to download. Set to self.image_dl_per_search.
            Args:
                num_image (int): num of image to download.
        """
        self.image_dl_per_search = num_image

    def get_searchlist_fr_file(self, filename):
        """Get search list from filename. Ability to add in a lot of phrases.
            Will replace the self.g_search_key_list
            Args:
                filename (str): full file path
        """
        with open(filename,'r') as f:
            self.g_search_key_list = f.readlines()

    def formed_search_url(self):
        ''' Form the url either one selected key phrases or multiple search items.
            Get the url from the self.g_search_key_list
            Set to self.sp_search_url_list
        '''
        self.reformat_search_for_spaces()
        self.target_url_str = self.prefix_of_search_url + self.g_search_key +\
                                self.postfix_of_search_url

    def retrieve_source_fr_html(self):
        """ Make use of selenium. Retrieve from html table using pandas table.

        """
        #binary = FirefoxBinary(r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe')
        #driver = webdriver.Firefox(firefox_binary=binary)  # webdriver.Firefox()
        self.driver.get(self.target_url_str)
        self.driver.execute_script("document.body.style.zoom='20%'")

        print (self.target_url_str)
        ## wait for log in then get the page source.
        try:
            for k in range(4):
                try:
                    text = "More images"
                    self.driver.find_element_by_xpath("(//*[contains(text(), '" + text + "')] | //*[@value='" + text + "'])").click()
                except:
                    print('did not find button')
                    pass
                # driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
                self.driver.execute_script("window.scrollBy(0, 1000)")
                time.sleep(2)
                divs = self.driver.find_elements_by_xpath("(//div)")
                for div in divs:
                    bem = div.get_attribute("data-bem")
                    if bem is not None:
                        json_data = json.loads(bem)
                        if 'serp-item' in json_data:
                            self.pic_url_list.append((json_data['serp-item']['img_href']))
                print ('found %d divs' % len(divs))
                self.pic_url_list = list(set(self.pic_url_list))
                print ('%d current elements' % len(self.pic_url_list))
                if len (self.pic_url_list)>200:
                    break

        except:
            print ('not able to find')


    def extract_pic_url(self):
        """ extract all the raw pic url in list

        """
        dom = DOM(self.page_source)
        tag_list = dom('div')
        print('parsing %d elements' % len(tag_list))
        count = 0
        for tag in tag_list[:self.image_dl_per_search]:
            if 'data-bem' in tag.attributes:
                strr = tag.attributes['data-bem']
                json_data = json.loads(strr)
                if 'serp-item' in json_data:
                    self.pic_url_list.append((json_data['serp-item']['img_href']))
                    count += 1

        print('added %d elements' % count)
        self.pic_url_list = list(set(self.pic_url_list))
        print ('current size %d' % len(self.pic_url_list))


    def multi_search_download(self):
        """ Mutli search download"""
        for indiv_search in self.g_search_key_list:
            self.pic_url_list = []
            self.pic_info_list = []

            self.g_search_key = indiv_search

            self.formed_search_url()
            self.retrieve_source_fr_html()
            #self.extract_pic_url()
            self.downloading_all_photos() #some download might not be jpg?? use selnium to download??
            self.save_infolist_to_file('list/yandex/%d.csv'%self.g_search_id)

    def already_done (self):
        return os.path.isfile ('list/%d.csv'%self.g_search_id)

    def downloading_all_photos(self):
        """ download all photos to particular folder

        """
        self.create_folder()
        pic_counter = 1
        for url_link in self.pic_url_list:
            #print ('%d %s' % (pic_counter, url_link))
            pic_prefix_str = self.g_search_key  + str(pic_counter)
            self.download_single_image(url_link.encode(), pic_prefix_str)
            pic_counter = pic_counter +1

    def download_single_image(self, url_link, pic_prefix_str):
        """ Download data according to the url link given.
            Args:
                url_link (str): url str.
                pic_prefix_str (str): pic_prefix_str for unique label the pic
        """
        self.download_fault = 0
        file_ext = os.path.splitext(url_link)[1] #use for checking valid pic ext
        temp_filename = pic_prefix_str + file_ext
        temp_filename_full_path = os.path.join(self.gs_raw_dirpath, temp_filename )

        valid_image_ext_list = ['.png','.jpg','.jpeg', '.gif', '.bmp', '.tiff'] #not comprehensive

        #url = URL(url_link)
        #if url.redirect:
        #    return # if there is re-direct, return

        #if file_ext not in valid_image_ext_list:
        #    return #return if not valid image extension

        #print (url_link)
        print (urllib2.unquote(url_link))

        #self.pic_info_list.append(pic_prefix_str + ': ' + urllib2.unquote(url_link ))
        self.pic_info_list.append(urllib2.unquote(url_link ))

        return

        f = open(temp_filename_full_path, 'wb') # save as test.gif
        print ('writing to %s' % temp_filename_full_path)
        basedir=os.path.dirname(temp_filename_full_path)
        if not os.path.isdir(basedir):
            os.makedirs(basedir)
        try:
            f.write(url.download())#if have problem skip
        except:
            #if self.__print_download_fault:
            print ('Problem with processing this data: ', url_link)
            self.download_fault =1
        f.close()

    def create_folder(self):
        """
            Create a folder to put the log data segregate by date

        """
        self.gs_raw_dirpath = os.path.join(self.folder_main_dir_prefix, time.strftime("_%d_%b%y", time.localtime()))
        if not os.path.exists(self.gs_raw_dirpath):
            os.makedirs(self.gs_raw_dirpath)

    def save_infolist_to_file(self,filename):
        """ Save the info list to file.

        """
        #temp_filename_full_path = filename#os.path.join(self.gs_raw_dirpath, self.g_search_key + '_info.txt' )
        #temp_filename_full_path = '_info.txt'

        print('Saving %d elements' % len(self.pic_info_list))
        basedir = os.path.dirname(filename)
        if not os.path.isdir(basedir):
            os.makedirs(basedir)

        with  open(filename, 'w') as f:
            for n in self.pic_info_list:
                f.write(n)
                f.write('\n')

        # send to AWS
        cmd = 'C:\\pscp.exe %s ubuntu@52.212.202.91:/ops/lsid/list/yandex/' % filename
        os.system(cmd)
        success_filename = filename + '.success'
        with open(success_filename,'w'):
            pass
        cmd = 'C:\\pscp.exe %s ubuntu@52.212.202.91:/ops/lsid/list/yandex/' % success_filename
        os.system(cmd)

def main():
    """main function
    """
    print (os.environ['PATH'])

    # read taxonomy
    tax = [a.split(',') for a in open('taxonomy.csv','r').readlines()]
    tax = [(a[1],a[-2]) for a in tax]
    tax = tax[1:]

    w = GoogleImageExtractor()  # leave blanks if get the search list from file
    w.set_num_image_to_dl(1000)

    for (t,count) in zip(tax,range(len(tax))):
        category_name = t[1]
        category_id = int(t[0])
        category_name = category_name.replace('&',' and ')
        category_name = re.sub('[^a-zA-Z\s]', '', category_name).strip()

        print ('%d/%d : %s/%d' % (count,len(tax),category_name,category_id))
        #if os.path.isfile ('list/%d.csv'%category_id):
        #    continue

        #searchlist_filename = 'list.txt'
        #w.get_searchlist_fr_file(searchlist_filename)#replace the searclist
        w.setCategory (category_name)
        w.setCategoryId (category_id)
        w.multi_search_download()

        #if count==0:
        #    break
#        except:
#            print('ERROR: failed to process category %d' % category_id)
        #if count==3:
        #    break

if __name__ == '__main__':
    #cmd = 'C:\\pscp.exe C:\\ticket.pdf ubuntu@52.212.202.91:/home/ubuntu'
    main()
