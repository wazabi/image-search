import glob
import os
import glob
import shutil
import sys
sys.path.append('../')
import util
import numpy as np

def count_images (input_dir):
    dirs = util.list_dirs (input_dir)
    counts= np.array([])
    count_dict = {}
    total_count=0
    for dirname in dirs:
        files = glob.glob(dirname+'/*.jp*g')
        nfiles = len(files)
        count_dict[dirname.split('/')[-1]]=nfiles
        counts = np.append(counts,nfiles)
        total_count += nfiles

    thresh=100
    res = []
    for k,v in count_dict.iteritems():
        if v<thresh:
#            print ('Warning : dir %s has %d < %d images' % (k,v,thresh))
            res.append (int(k))
    return res


def replay (input_dir):
    # replay small directories
    categories = count_images (input_dir)
    for cat in categories:
        for source in ['yahoo','yandex','google']:
            filename ='/ops/lsid/list/%s/%d.csv.done' % (source, cat)
            if not os.path.isfile(filename):
                continue
            new_name = filename.replace('.done','.success')
            print ('%s --> %s' % (filename, new_name))
            shutil.move (filename,new_name)


def remove_weird_files ():
    count=0
    # remove weird files
    #for filename in glob.glob('/oph/images/*/*.jp*g'):
    for filename in glob.glob('/ops/lsid/images/*/*.jp*g'):
        if filename.find('-') != -1:
            print (filename)
            count+=1
            os.remove(filename)
    print(count)

if __name__ == "__main__":

        remove_weird_files ()
        #replay ('/ops/lsid/images')
