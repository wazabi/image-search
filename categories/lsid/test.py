import os
import glob
import argparse
import multiprocessing
import requests
import shutil
import re
import time
import random
import imghdr
import subprocess as sp
import datetime
from pid import PidFile

def error_log (msg):
    timestamp_str = datetime.datetime.now().strftime("%B %d, %Y -- %I:%M%p")
    print ('%s : %s' % (timestamp_str,msg))
    with open ('/ops/err','a') as gp:
        gp.write ('%s : %s\n' % (timestamp_str,msg))

def list_sources (inputdir):
    return filter (lambda a : os.path.isdir(os.path.join(inputdir,a)), os.listdir(inputdir))

def list_categories (inputdir, sources):
    files = []
    for s in sources:
        files += [os.path.basename(a) for a in glob.glob('%s/*.csv' % os.path.join(inputdir,s))]
    files = list(set(files))
    return [int(a.split('.')[0]) for a in files]

def list_files (inputdir):
    return glob.glob ('%s/*/*.csv' % inputdir)

def list_latest_files (inputdir):
    """ Find latest success file
    """
    success_files = glob.glob ('%s/*/*.csv.success' % inputdir)
    dates = []
    for filename in success_files:
        st = os.stat (filename)
        btime = st.st_mtime
        dates.append ((filename,btime))
    if len(dates)==0:
        return []
    dates = sorted (dates, key=lambda a:a[1])
    return [a[0].replace('.success','') for a in dates]
#    return dates[0][0].replace('.success','')


def reset_dir (dirname):
    """ Erase and re-create a directory
    """
    if os.path.isdir (dirname):
        shutil.rmtree (dirname)
    os.makedirs (dirname)


def convert_to_jpeg (filename, img_type):
    (root,ext) = os.path.splitext (filename)
    print ('converting %s (%s)' % (filename, img_type))
    real_filename = root + '.' + img_type
    os.rename (filename, real_filename)
    new_filename = root + '.jpg'
    cmd = 'convert %s %s' % (real_filename, new_filename)
    print ('check cmd: %s' % cmd)
    os.system (cmd)

def file_to_urls (filename):
    basename = os.path.basename(filename)
    category = int (basename.split('.')[0])
    source = filename.split('/')[-2]
    with open (filename,'r') as fp:
        lines = [a.strip() for a in fp.readlines()]
        lines_r = zip (lines,range(len(lines)))
    return [(source,category,k,url) for (url,k) in lines_r]

def build_job_list (urls, n_proc, maxn):
    """ Build a list of URLs to download
    """
    joblist = [[] for j in range(n_proc)]
   
    random.shuffle (urls)

    for k in range(len(urls)):
        joblist[k%n_proc].append (urls[k])

    print([len(jobs) for jobs in joblist])
    
    return joblist


def download_image (url, procid, filename):
    """ download an image from the web using requests
    """
    success=False
    tmp_dir='/tmp/down-%d' % procid
    reset_dir (tmp_dir)
    FNULL = open(os.devnull,'w')
    ERRLOG = open('/tmp/err','w')
    #proc = sp.Popen(['wget','--no-check-certificate','-U','Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0','-P',tmp_dir,url], stdout=FNULL, stderr=FNULL)
    proc = sp.Popen(['wget','--no-check-certificate','-T','2','-U','firefox','-P',tmp_dir,'--tries','3',url], stdout=FNULL, stderr=FNULL)
    proc.wait()
    if proc.returncode == 0:
        wfilename = filter (lambda a : os.path.isfile(os.path.join(tmp_dir,a)), os.listdir(tmp_dir))
        if len(wfilename)==1:
            wfilename = wfilename[0]
            wfullname = os.path.join (tmp_dir, wfilename)
            ext = os.path.splitext (wfilename)[1]
            if ext == '.jpeg' or ext == '.jpg':
                shutil.move(wfullname, filename)
                success=True
            else:
                proc = sp.Popen(['convert',wfullname,filename], stdout=ERRLOG, stderr=ERRLOG)
                proc.wait()
                success = (proc.returncode==0)
                if not success:
                    error_log ('ERROR running convert command %s --> %s. Return code : %d' % (wfullname, filename, proc.returncode))
        else:
            error_log ('ERROR : no single file in %s after wget on %s' % (tmp_dir, url))
    else:
        error_log('ERROR : wget failed on URL %s (return code : %d)' % (url, proc.returncode))
    return success


def parallel_run (output_dir, procid, job_list):
    """ download images
    """
    icount=0
    for j in job_list:
        source = j[0]
        category = j[1]
        count = j[2]
        url = j[3]
        local_outdir = os.path.join (output_dir, '%d/' % category)
        if not os.path.isdir (local_outdir):
            os.makedirs (local_outdir)
        out_filename = os.path.join (local_outdir, '%s_%d.jpg'%(source, count))
        # do not download same image twice
        #if os.path.isfile (out_filename):
        #    continue
        #print ('%s --> %s...' % (url, out_filename))
        print ('%s...' % (out_filename))
        if not download_image (url, procid, out_filename):
            error_log ('ERROR : failed to download image from URL %s' % url)
        icount+=1
        #if icount==30:
        #    break

def execute_list (job_lists, output_dir):
    """ Execute a job list
    """
    n_proc = len(job_lists)
    jobs = []
    for proc in range(n_proc):
        process = multiprocessing.Process (target=parallel_run, args=[output_dir, proc, job_lists[proc]])
        process.start()
        jobs.append(process)

    # wait for all
    for job in jobs:
        job.join()


def count_images (indir):
    filelist = glob.glob(indir+'/*/*.jpg')
    return (len (filelist))


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory', required=True)
    parser.add_argument('--nproc', dest='n_procs', type=int, help='number of processes to launch', required=False, default=32)
    parser.add_argument('--maxn', dest='maxn', type=int, help='max number of items per category', required=False, default=5000)
    args = parser.parse_args()

    #files = list_files (args.input_dir)
    #files = list_latest_files (args.input_dir)
    #print (files)

    # cap batches
    #files = files[:50]

    files = ['/ops/lsid/list/yahoo/315.csv']
    files = ['/ops/lsid/list/google/6076.csv']

#    files = [files]

    urls = []
    for f in files:
        urls += file_to_urls (f)
    print ('%d urls to download over %d files' % (len(urls),len(files)))
    
    # build job list
    job_lists = build_job_list (urls, args.n_procs, args.maxn)

    # execute
    execute_list (job_lists, args.output_dir)

    # count images
    #n_images = count_images (args.output_dir)
    #print ('output dir has %d images.' % n_images)

    # archive files
    #for f in files:
    #    shutil.move (f+'.success',f+'.done')

if __name__ == "__main__":
    with PidFile ():
        main()
