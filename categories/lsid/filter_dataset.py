import argparse
import glob
import os
import random
import re
import sys
sys.path.append('../')
import util
import subprocess
import time

def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory containing images', required=True)
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory containing ** links ** to images', required=True)
    parser.add_argument('--filter', dest='filter_file', type=str, help='list of categories to keep', required=False)
    parser.add_argument('--maxn', dest='maxn', type=int, help='max number of items per source of images', required=False, default=3000)
    args = parser.parse_args()

    # read whitelist
    if args.filter_file is not None:
        with open (args.filter_file) as fp:
            categories = [int(a.strip()) for a in fp.readlines()]
    
        random.shuffle (categories)
        #categories = categories[:100]

    # list images
    files = glob.glob (os.path.join (args.input_dir, '*/*.jp*g'))
    print (len(files))

    random.shuffle(files)

    #files = files[:100]

    # filter files with weird names
    if False:
        t0 = time.time()
        filter_func = lambda a : a.find('-') == -1
        files = filter (filter_func, files)
        t1 = time.time()
        print ('%.3f elapsed seconds' % (t1-t0))
        print (len(files))

    # filter on categories
    if args.filter_file is not None:
        filter_func = lambda a: int(a.split('/')[-2]) in categories
        t0 = time.time()
        files = filter (filter_func, files)
        t1 = time.time()
        print ('%.3f elapsed seconds' % (t1-t0))
        print (len(files))

    # filter on image ID
    if False:
        filter_func = lambda a: int(re.split('/|\.|_',a)[-2]) < args.maxn
        for f in files:
            try :
                filter_func (f)
            except:
                print ('ERROR failed to parse filename %s' % f)

        t0 = time.time()
        files = filter (filter_func, files)
        t1 = time.time()
        print ('%.3f elapsed seconds' % (t1-t0))
        print (len(files))

    # filter on image readability by tensorflow (using djpeg as a proxy)
    # this filter is here because tensorflow fails to read malformed/buggy JPEG images
    if False:
        t0 = time.time()
        files = filter (util.check_jpeg_file, files)
        t1 = time.time()
        print ('%.3f elapsed seconds' % (t1-t0))
        print (len(files))

    # create symlinks
    util.reset_dir (args.output_dir)

    for filename in files:
        partial_name = '/'.join(filename.split('/')[-2:])
        link_name = os.path.join (args.output_dir, partial_name)
        link_dirname = os.path.dirname (link_name)
        if not os.path.isdir (link_dirname):
            os.makedirs (link_dirname)
        print ('%100s --> %50s' % (filename, link_name))
        os.symlink (filename, link_name)

if __name__ == "__main__":
    main()

