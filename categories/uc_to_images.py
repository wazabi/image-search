import argparse
import glob
import os
import gzip
import json
import util
import random

def process_file (filename):
    """ Read a gzipped UC catalog json file
    as produced by get_universal_catalog.py
    """
    output = []
    print ('%s...' % filename)
    failed=0
    total=0
    with gzip.open(filename, 'rb') as f:
        partnerid=int(os.path.basename(filename).split('-')[0])
        file_content = f.read()
        for line in file_content.split('\n'):
            if len(line.strip())==0:
                continue
            data = json.loads (line)
            reallyRecommendable=data['reallyRecommendable']
            if not reallyRecommendable:
                continue
            internalid = data['internalId']
            total+=1
            try:
                category = data['categoryEnrichment']['googleCategoryId']
            except:
                failed+=1
                continue
            url1 = data['bigimage']
            url2 = None
            try:
                url2 = data['smallimage']
            except:
                pass
            output.append ((partnerid,internalid,category,url1,url2))
    print ('failure rate: %.2f%%' % (100.0*failed/total))
    return output


def remove_duplicates (data):
    """ Remove duplicates from data.
    A duplicate is defined as same partnerid, same internalid
    """
    fdata = []
    d_pi = {}
    for d in data:
        partnerid = d[0]
        internalid = d[1]
        if partnerid in d_pi and internalid in d_pi[partnerid]:
            continue
        if not partnerid in d_pi:
            d_pi[partnerid]=[]
        d_pi[partnerid].append(internalid)
        fdata.append (d)
    return fdata


def data_to_csv (data, data_dir, max_lines):
    filecount=0
    fileh=open(os.path.join(data_dir,'part-%05d.csv'%filecount),'w')
    linecount=0
    for d in data:
        url = d[3]
        if url is None:
            url = d[4]
        if url is None:
            continue
        fileh.write('((%d,%d),(%d,%s))\n'% (d[1],d[0],d[2],url))
        linecount+=1
        if linecount==max_lines:
            fileh.close()
            filecount+=1
            fileh=open(os.path.join(data_dir,'part-%05d.csv'%filecount),'w')
            linecount=0


def main ():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='dataset directory', required=True)
    args = parser.parse_args()
   
    #list files
    data = []
    files = glob.glob (os.path.join(args.input_dir,'*.gz'))
    print ('%d files to read.' % len(files))
    for filename in files:
        data += process_file (filename)
    
    print ('%d items in database' % len(data))

    # remove duplicates
    data = remove_duplicates (data)

    print ('%d items in database (after duplicate removal)' % len(data))

    # shuffle data to make download easier
    random.shuffle (data)

    # store data to files
    data_dir = os.path.join(args.input_dir,'csv')
    util.reset_dir (data_dir)

    # data to files
    max_lines = 1000
    data_to_csv (data, data_dir, 1000)



if __name__ == "__main__":

    main()
