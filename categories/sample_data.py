import util
import glob
import os
import time
import numpy as np
import multiprocessing
import logging
import argparse

_logger = None

def benchmark_reading_time ():
    input_train_dir = '/opt/extractFull'

    ops = []
    opc = []

    avg_time = .0
    avg_count = 0
    for filename in glob.glob (os.path.join(input_train_dir,'part-*')):
        start = time.clock()
        (cnn_vectors, categories, productids, partnerids) = util.read_data_from_file (filename)
        dur = time.clock() - start
        avg_time += dur
        avg_count += 1
        if avg_count>10:
            break
    print ('average time : %.5f sec.' % (avg_time / avg_count))

    input_train_dir = '/opt/extractFullShuffledTrain'

    ops = []
    opc = []

    avg_time = .0
    avg_count = 0
    for filename in glob.glob (os.path.join(input_train_dir,'part-*')):
        start = time.clock()
        x = np.load(filename)
        dur = time.clock() - start
        avg_time += dur
        avg_count += 1
        if avg_count>10:
            break
    print ('average time : %.5f sec.' % (avg_time / avg_count))


def sample_data(input_dir, output_dir, minn, categ2key, whitelist):
    """ Sample a dataset in a directory in order to have at least <minn> examples and at most <10 x minn> examples per category
   
        whitelist : whitelist of categories
        """
    global _logger

    MAXN_PER_FILE = 10000 # max number of lines per file

    # reset output dir
    util.reset_dir (output_dir)

    filecount=0
    X = None
    filelist = glob.glob (os.path.join(input_dir,'part-*'))
    nfiles = len(filelist)

    # sample data
    s_categ_count = {}
    for (count,filename) in zip(range(nfiles),filelist):
        _logger.info ('[%5d/%5d] %40s' % (count,nfiles,filename))
        x = np.load(filename)
        # mask rows
        keeprows = []
        for row in range(x.shape[0]):
            categ = int(x[row,-3])
            categ = categ2key[categ]
            if (not categ in whitelist) or (categ in s_categ_count and s_categ_count[categ]>=10*minn):
                continue
            if not categ in s_categ_count:
                s_categ_count[categ]=1
            else:
                s_categ_count[categ]+=1
            keeprows.append(row)
        # append to RAM
        if len(keeprows)>0:
            if X is None:
                X = x[keeprows,:]
            else:
                X = np.vstack((X,x[keeprows,:]))
        else:
            continue
        # dump to file
        if X.shape[0]>MAXN_PER_FILE:
            out_filename = os.path.join(output_dir,'part-%05d'%filecount)
            filecount+=1
            _logger.info('\tdumping data %d x %d to file %s' % (X.shape[0], X.shape[1], out_filename))
            np.save(out_filename, X)
            X = None
        # measure progress
        mink = -1
        minc = 1E10
        for k,v in s_categ_count.iteritems():
            if v < minc:
                minc = v
                mink = k
        if minc<1E10 and minc<minn:
            _logger.info('still missing examples. worse category %d with %d examples.' % (mink, minc))
        if X is not None:
            _logger.info ('current shape %d x %d' % (X.shape[0],X.shape[1]))
        # debug : save to file
        with open ('s_categ_count.csv','w') as gp:
            for k,v in s_categ_count.iteritems():
                gp.write('%d,%d\n' % (k,v))
    if X is not None:
        out_filename = os.path.join(output_dir,'part-%05d'%filecount)
        filecount+=1
        np.save(out_filename, X)
        X = None
    _logger.info('wrote %d files.' % filecount)


def set_logger ():
    global _logger

    # init logger
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT)
    _logger = logging.getLogger('learning')
    _logger.setLevel(logging.INFO)
    fh = logging.FileHandler ('log_sample')
    fh.setLevel (logging.INFO)
    formatter = logging.Formatter (FORMAT)
    fh.setFormatter (formatter)
    _logger.addHandler(fh)


def main():
    # compute categories distribution
    #input_train_dir = '/opt/extractFullShuffledTrain'
    #categories_distribution (input_train_dir, -1, '/opt/categories_distribution.csv')
    #return
    global _logger

    set_logger ()

    parser = argparse.ArgumentParser()
    parser.add_argument('--indirbase', dest='input_dir_base', type=str, help='input directory', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy (use leaf by default)', required=True)
    args = parser.parse_args()

    input_train_dir = args.input_dir_base+'Train'
    output_train_dir = args.input_dir_base+'TrainSampled'
    input_test_dir = args.input_dir_base+'Test'
    output_test_dir = args.input_dir_base+'TestSampled'

    if not os.path.isdir (input_train_dir):
        _logger.info ('Error : Directory %s does not exist.' % input_train_dir)
        return
    if not os.path.isdir (input_test_dir):
        _logger.info ('Error : Directory %s does not exist.' % input_test_dir)
        return

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    _logger.info("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name, _) = util.taxonomy_map_at_depth (tax, depth)
    
    # stats categories
    categories_dist_file = os.path.join (input_train_dir,'categories-distribution-%s.csv'%depth)
    if os.path.isfile (categories_dist_file):
        categ_dist = util.read_dict (categories_dist_file)
    else:
        _logger.info('computing categories distribution from %s...' % input_train_dir)
        categories = util.list_categories (input_train_dir, categ2key)
        categ_dist = util.dist_stats (categories)
        util.dict_to_file (categ_dist, categories_dist_file)

    # build whitelist
    whitelist = []
    minn = 1000
    for k,v in categ_dist.iteritems():
        if v > minn:
            whitelist.append(k)
    _logger.info ('%d categories in whitelist.' % len(whitelist))

    # sample data
    sample_data(input_train_dir, output_train_dir, minn, categ2key, whitelist)
    sample_data(input_test_dir, output_test_dir, minn, categ2key, whitelist)



if __name__ == "__main__":
    main()


