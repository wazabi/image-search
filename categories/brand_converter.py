import glob
import os
import re
import util
import json
import numpy as np

def filter_json (file_in, file_out, valid_strings):
    """ Filter a json file by keeping only really recommendable products
    and removing invalid google categories
    """
    assert(file_in != file_out)
    n_trial=0
    n_ok=0
    n_pred_ok=0
    with open (file_in,'r') as gp:
        hp = open(file_out,'w')
        lines = gp.readlines()
        for line in lines:
            n_trial += 1
            data = json.loads(line)
            if not data['reallyRecommendable']:
                continue
            if not 'googleProductCategory' in data:
                continue
            b_category = data['googleProductCategory'].split('>')[-1].lower().replace(' ','')
            if not b_category in valid_strings:
                continue
            partnerid = data['partnerId']
            productid = data['internalId']
            url = data['bigimage']
            category = valid_strings[b_category]
            out_line = '((%s,%s),(%d,%s))' % (productid, partnerid, category, url)
            hp.write('%s\n'%out_line)
            predictedid = data['categoryEnrichment']['googleCategoryId']
            if predictedid==category:
                n_pred_ok+=1
            n_ok += 1
        hp.close()
    read_rate = 1.0 * n_ok / n_trial
    pred_rate = None
    if n_ok > 0:
        pred_rate = 1.0 * n_pred_ok / n_ok
    return (read_rate, pred_rate)

    
def main():
    # load google taxonomy
    tax = util.load_taxonomy ()

    # convert to strings
    tax_str = util.taxonomy_to_strings (tax)

    root_dir = '/ops/brand'
    partids = {}
    accuracies = np.array([])
    read_rates = np.array([])
    for filename in glob.glob(os.path.join(root_dir,'*.gz')):
        idx = re.findall('\d+',filename)
        partnerid = int(idx[0])
        if not partnerid in partids:
            partids[partnerid]=0
        else:
            partids[partnerid]+=1
        partid = partids[partnerid]
        # create output directory for this partner
        out_dir = os.path.join (root_dir,'%d'%partnerid)
        if not os.path.isdir (out_dir):
            os.makedirs (out_dir)
        # unzip
        tmp_json_file = '/tmp/tmp.json'
        cmd = 'gzip -dc %s > %s' % (filename, tmp_json_file)
        os.system(cmd)
        # filter
        json_file = os.path.join(out_dir,'part-%05d.json'%partid)
        (read_rate, accuracy) = filter_json (tmp_json_file, json_file, tax_str)
        read_rates = np.append (read_rates, read_rate)
        if accuracy is not None:
            accuracies = np.append(accuracies, accuracy)
            print ('mean acc = %10.4f, read_rate = %10.4f' % (np.mean(accuracies), np.mean(read_rates)))

if __name__ == "__main__":
    main()

