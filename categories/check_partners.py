import os
import sys
sys.path.append(os.path.abspath('../'))
import hadoop_utils as hp
import json

def download_latest_part_sample (partnerid, output_file):
    """ download a sample of latest partner's data from HDFS
    """
    pathdir = '/user/wca/langoustine_universal/brand.out/%d' % partnerid
    dirs = hp.hdfsLs(pathdir)
    dirs = sorted (dirs, key=lambda a: a.modification_date, reverse=True)
    for d in dirs:
        print ('checking %s' % d.filename)
        pathdir = d.filename
        files = hp.hdfsLs(pathdir)
        files = sorted (files, key=lambda a : a.filesize)
        print (files)
        hp.hdfsCpToLocal (output_file,files[0].filename)
        break


def check_sample (tax_str, file_json):
    """ check that a sample dataset has valid google categories
    """
    n_ok = 0
    n_not_ok = 0
    with open (file_json,'r') as fp:
        lines = fp.readlines()
        for line in lines:
            data = json.loads (line)
            if not 'googleProductCategory' in data:
                continue
            google_category = data['googleProductCategory']
            google_category = google_category.strip().replace(' ','')
            if not google_category in tax_str:
                print ('%s not in the list' % google_category)
                n_not_ok += 1
            else:
                n_ok += 1
    print (n_ok, n_not_ok)
    return True


def check_partner (tax_str, partnerid):


    # download a sample of data
    sample_gz = 'part.gz'
    #download_latest_part_sample (partnerid, sample_gz)

    tmp_json = 'part.json'

    #os.system ('gzip -cd %s >%s' % (sample_gz, tmp_json))
    
    check_sample (tax_str, tmp_json)

    # uncompress


def main():
    # load google taxonomy strings
    strings_filename = 'taxonomy_strings.csv'
    with open (strings_filename) as gp:
        tax_str = [a.strip() for a in gp.readlines()]

    partnerid  = 9049
    check_partner (tax_str, partnerid)

if __name__ == "__main__":
    main()

