import argparse
import util
import glob
import os

def main():
    """ convert a dataset from leaf level to a given depth
    """
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy', required=True)
    args = parser.parse_args()

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    print("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name , _) = util.taxonomy_map_at_depth (tax, depth)

    # list and count images
    dirs = util.list_dirs (args.input_dir)
    categ_files={}
    for dirname in dirs:
        basename = os.path.basename (dirname)
        key = categ2key[int(basename)]
        files = glob.glob(dirname+'/*.jp*g')
        if not key in categ_files:
            categ_files[key] = files
        else:
            categ_files[key] += files

    # reset output
    util.reset_dir (args.output_dir)

    for k,files in categ_files.iteritems():
        print(k)
        relative_dirname = '%d' % k
        dirname = os.path.join(args.output_dir,relative_dirname)
        if not os.path.isdir (dirname):
            os.makedirs (dirname)
        for filename in files:
            basename = os.path.basename(filename)
            link_name = os.path.join (dirname, basename)
            os.symlink (filename, link_name)
            print ('%s --> %s' % (link_name, filename))
    
if __name__ == "__main__":
    main()


