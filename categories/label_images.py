import argparse
import util
import glob
import numpy as np
import os
import random
import shutil

def html_header (html_files):
    res = "<html><head> <link rel=\"stylesheet\" href=\"http://52.212.202.91/styles.css\"></head><body>"
    if len(html_files)>1:
        res += 'Page : '
        for k in range(len(html_files)):
            res += '[<a href="index_%d.html">%d</a>]&nbsp;\n' % (k, k)
    res += "<form action=\"http://52.212.202.91/action.php\" method=\"post\">"
    return res


def html_footer ():
    res = "<input name=\"Submit\" type=\"submit\" value=\"Submit\"/>"
    res += '</form></body></html>'
    return res


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy', required=True)
    parser.add_argument('--nfiles', dest='n_html_files', type=int, help='number of HTML files in output', required=False, default=1)
    args = parser.parse_args()

    maximages = 10

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    print("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name , _) = util.taxonomy_map_at_depth (tax, depth)

    # list and count images
    dirs = util.list_dirs (args.input_dir)
    categ_count={}
    categ_files={}
    for dirname in dirs:
        basename = os.path.basename (dirname)
        key = categ2key[int(basename)]
        files = glob.glob(dirname+'/*.jp*g')
        categ_count[key] = len(files)
        categ_files[key] = files

    # reset output
    util.reset_dir (args.output_dir)

    html_files = [os.path.join(args.output_dir,'index.html')]
    if args.n_html_files > 1:
        html_files = [os.path.join(args.output_dir,'index_%d.html') % d for d in range(args.n_html_files)]

    # generate www
    for html_file in html_files:
        with open (html_file,'a') as hp:
            hp.write (html_header(html_files))

    for k,count in categ_count.iteritems():
        print(k)
        fileid = k%len(html_files)
        html_file = html_files[fileid]
        with open (html_file,'a') as hp:
            hp.write ("<h2>%s (%d)</h2>\n"%(key2name[k],k))
            hp.write ("<input type=\"radio\" name=\"%d_%d\" value=\"1\" /checked><b>YES</b>\n"%(fileid,k))
            hp.write ("<input type=\"radio\" name=\"%d_%d\" value=\"0\"><b>NO</b>\n"%(fileid,k))

        relative_dirname = 'images/%d'%k
        dirname = os.path.join(args.output_dir,relative_dirname)
        util.reset_dir (dirname)
        files = categ_files[k]
        if count>maximages:
            random.shuffle(files)
            files = files[:maximages]
        for filename in files:
            #print ('%s --> %s' % (filename, dirname))
            #shutil.copy(filename,dirname)
            basename = os.path.basename(filename)
            cmd = 'convert %s -resize x150 %s' % (filename,os.path.join(dirname,basename))
            os.system(cmd)
            www_filename = os.path.join (relative_dirname,basename)
            with open (html_file,'a') as hp:
                hp.write ("<img src=\"%s\" height=\"150\">\n" % www_filename)
    
    for html_file in html_files:
        with open (html_file,'a') as hp:
            hp.write (html_footer())
    
if __name__ == "__main__":
    main()


