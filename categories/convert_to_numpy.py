import os
import glob


def convert_to_numpy (input_dir, do_remove):
    """ Convert a list of files to numpy-readable files
    """
    filelist = list(glob.glob(os.path.join(input_dir,'part-*')))
    count=0
    nfiles = len(filelist)
    for filename in filelist:
        basename = os.path.basename (filename)
        out_filename = os.path.join (input_dir, basename+'.npy')
        (cnn, cat, productids, partnerids) = read_data_from_file (filename)
        vcat = cat.reshape((cat.size,1))
        vproductids = productids.reshape((cat.size,1))
        vpartnerids = partnerids.reshape((cat.size,1))
        x = np.hstack ((cnn, vcat,vproductids,vpartnerids))
        np.save (out_filename, x)
        if do_remove:
            os.remove (filename)
        count+=1
        print ("[%05d / %05d] %s --> %s" % (count, nfiles, filename, out_filename))


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    args = parser.parse_args()


    print ("converting data to numpy...")
    util.convert_to_numpy (args.input_dir, True)


if __name__ == "__main__":
    main()


