#!/bin/bash
#for epochs in 1 5 10 20 50
for epochs in 120
do
    #for depth in Level1 Level2 Level3 Level5 leaf
    for depth in Level2
    do
        #cmd="python learn_categories.py --epochs $epochs --depth $depth --logdatafile out-$epochs-$depth.csv --batches 1 --valid 1"
        #echo $cmd
        #eval $cmd
        cmd="python learn_categories.py --epochs $epochs --depth $depth --logdatafile out-$epochs-$depth.csv --batches -1 --valid 1"
        echo $cmd
        eval $cmd
    done
done
