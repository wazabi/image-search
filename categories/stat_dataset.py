import numpy as np
import glob
import argparse
import os

def compute_mean (datadir):
    """ Compute mean of a dataset
    """
    a_mean = None
    nel=0
    filelist = glob.glob (os.path.join(datadir,'part-*'))
    for filename in filelist:
        print ('\t%s...' %filename)
        x = np.load(filename)
        x = x[:,:-3]
        mu = np.sum(x,axis=0)
        nel += x.shape[0]
        if a_mean is None:
            a_mean = mu
        else:
            a_mean += mu
    return a_mean / nel


def compute_std (datadir, a_mean):
    """ Compute standard deviation of a dataset given its mean
    """
    a_var = None
    nel = 0
    filelist = glob.glob (os.path.join(datadir,'part-*'))
    for filename in filelist:
        print ('\t%s...' %filename)
        x = np.load(filename)
        x = x[:,:-3]
        assert(a_mean.size==x.shape[1])
        for k in range(x.shape[1]):
            x[:,k] -= a_mean[k]
        var = np.square (x)
        var = np.sum(var,axis=0)
        nel += x.shape[0]
        if a_var is None:
            a_var = var
        else:
            a_var += var
    a_var /= nel
    assert(~np.any(a_var<0))
    return np.sqrt(a_var)


def main ():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', dest='datadir', type=str, help='dataset directory', required=True)
    args = parser.parse_args()

    print ('computing mean...')
    a_mean = compute_mean (args.datadir)
    print (a_mean)
    print ('computing std...')
    a_std = compute_std (args.datadir, a_mean)
    print (a_std)

    np.save(os.path.join(args.datadir,'a_mean.npy'),a_mean)
    np.save(os.path.join(args.datadir,'a_std.npy'),a_mean)


if __name__ == "__main__":

    main()
