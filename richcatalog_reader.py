#!/usr/bin/env python
# Hathi : l'elephant, le seigneur de la jungle et le Gardien des legendes, respecte par toutes les creatures. Il a le droit de vie ou de mort car c'est lui qui lance l'appel de la treve de l'eau.
#
# This script transfers features from AWS to HDFS
#
import os
import sys
import datetime
import subprocess
import pipes
import time
import shutil
import hadoop_utils
from util import util_hdfs_ls, util_send_email, util_timestamp_file_to_json, util_run_cmd, util_get_platform
import re
from pid import PidFile
import json

def _log (msg):
    dt_string = datetime.datetime.now().strftime ('%Y-%m-%d %H:%M:%S')
    filename = os.path.basename(__file__)  
    msg_string = '[%s]\t%s\t[%s]\t%s' % (util_get_platform(), dt_string, filename, msg)
   
    print (msg_string)

    with open('/home/o.koch/embeddings/_%s.log' % filename,'a') as fp:
        fp.write ('%s\n' % msg_string)
        fp.close()

    # log error messages (but ignore HDFS warnings)
    if msg.find ('ERROR') is not -1 and msg.find ('WARN retry.RetryInvocationHandler') == -1:
        with open('/home/o.koch/embeddings/_%s.log.error' % os.path.basename(__file__) ,'a') as fp:
            fp.write ('%s\n' % msg_string)
            fp.close()


def _take_lock ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    assert (not os.path.isfile (filename))
    with open (filename, 'w') as fp:
        fp.write('0')

def _is_locked ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    return os.path.isfile (filename)

def _release_lock ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    assert (os.path.isfile (filename))
    os.remove(filename)

def exists_remote(host, path):
    proc = subprocess.Popen(['ssh', host, 'test -f %s' % pipes.quote(path)])
    proc.wait()
    return proc.returncode == 0

def exists_hdfs(path):
    proc = subprocess.Popen(['hadoop','fs','-test','-d',path])
    proc.wait()
    return proc.returncode == 0


def move_aws_directory (host, src, dest):
    proc = subprocess.Popen(['ssh', host, 'mv %s %s' % (pipes.quote(src),pipes.quote(dest))])
    proc.wait()


def find_first_timestamp_remote(host, path):
    proc = subprocess.Popen(['ssh', host, 'ls -td -- %s/*/.success.akela | tail -n 1' % pipes.quote(path)],\
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    result = proc.stdout.readlines()
    if result == []:
        error = proc.stderr.readlines()
        _log ('%s' % error)
        return None
    return os.path.join (path, result[0].strip())


def list_remote_dirs (host, path):
    proc = subprocess.Popen(['ssh', host, 'ls -td -- %s/*/.success.akela' % pipes.quote(path)],\
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    result = proc.stdout.readlines()
    if result == []:
        error = proc.stderr.readlines()
        _log ('%s' % error)
        return None
    dirs = []
    for filename in result:
        dirs.append(os.path.dirname(filename.strip()))
    return dirs


def list_partners():
    # load list of partner ids to look for
    partnerlistfile = '/home/o.koch/embeddings/lastPartnerTimestamp.txt'
    with open(partnerlistfile,'r') as fh:
        d = fh.readlines()
        partners = [int(x.split(' ')[0]) for x in d]

    return partners


def read_json_file (json_file):
    """ read a json file and return pairs of external hash ids"""

    pairs = []
    with open(json_file,'r') as fp:
        for line in fp.readlines():
            try:
                input_data = json.loads(line)
            except:
                _log('*** ERROR *** Failed to read JSON file %s.  Exiting.' % json_file)
            key_external_id = input_data['external_id_hash_64']
            nsim = len(input_data['similarities'])
            for s in range(nsim):
                val_external_id = input_data['similarities'][s]['external_id_hash_64']
                pairs.append((key_external_id,val_external_id))

    return pairs

def pairs_to_csv (partnerid, pairs):
    """ write pairs into csv file"""
    csv_file = 'coevents/coevents-%d.csv' % partnerid
    with open (csv_file, 'w') as fp:
        for pair in pairs:
            fp.write('%d,%d\n' % (pair[0], pair[1]))

    return csv_file

def remove_aws_directory (host, dirname):
    proc = subprocess.Popen(['ssh', host, 'rm -rf %s' % (pipes.quote(dirname))])
    proc.wait()


def main():

    host = 'ubuntu@54.246.97.118'
   
    # read data from HDFS
    process_dir(host)

    # remove AWS directory
    _log ('Removing directory on AWS...')
    remove_aws_directory(host, '/opt/sales')

    # send coevents to AWS
    cmd = 'scp sales.txt %s:/opt' % host
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return
    cmd = 'scp nosales.txt %s:/opt' % host
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return

    #json_file = 'coevents.json'
    #pairs = read_json_file (json_file)
    #csv_to_file (1, pairs)


def read_output_by_partner (hdfs_path):

    json_filename = '/tmp/outputByPartner.richcatalog'

    # read file from HDFS
    cmd = 'hadoop fs -text %s > %s' % (hdfs_path, json_filename)
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return None
    
    # parse JSON data
    with open(json_filename,'r') as fp:
        json_data = fp.read()
        try:
            data = json.loads(json_data)
        except:
            _log('*** ERROR *** Failed to read JSON file %s.  File might be empty.  Exiting.' % json_filename)
            return None

    return data


def parse_json_output (data, hdfs_dir, partner_ids):
    output = []
    if data is None:
        _log ('*** WARNING *** Parsing null data')
        return
    for item in data:
        partner_id = int(item)
        if not partner_id in partner_ids:
            continue
        output_folder = data[item]['outputFolder']
        files = data[item]['files']
        for nfile in files:
            fullpath = os.path.join (hdfs_dir, output_folder, nfile)
            output.append (fullpath)
    return output


def list_partners_from_file (filename):
    partner_ids = []
    if os.path.isfile (filename):
        with open(filename,'r') as fp:
            lines = fp.readlines()
            for l in lines:
                l = l.strip().split(' ')
                partnerid = int(l[0])
                partner_ids.append (partnerid)
    return partner_ids


def read_hdfs_catalog_file (hdfs_path):
    temp_file = '/tmp/catalog.json'
    cmd = 'hadoop fs -text %s | java -cp /home/jp.lamyeemui/recocomputer/lib/criteo-hadoop-recocomputer.jar com.criteo.recommendation.babelfish.utils.ProtobufUtil \"EnrichedProduct\" > %s' % (hdfs_path, temp_file)
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return None
    return temp_file


def parse_catalog_file_split_sales (filename, output_sales_file, output_nosales_file):
    nosales_fp = open (output_nosales_file, 'a')
    sales_fp = open (output_sales_file, 'a')

    with open (filename,'r') as fp:
        lines = fp.readlines()
        for line in lines:
            try:
                data = json.loads(line)
            except:
                _log('*** ERROR *** Failed to read JSON file %s.  File might be empty.  Exiting.' % json_filename)
                return None
            nb_views = int(data['popularity']['nb_views_30d'])
            nb_sales = int(data['popularity']['nb_sales_30d'])
            external_item_id = int(data['external_item_id_hash_64'])
            partner_id = int(data['partner_id'])
            if nb_views == 0:
                continue
            if (nb_sales == 0):
                nosales_fp.write ('%d,%d,%d\n' % (partner_id, external_item_id, nb_sales))
            else:
                sales_fp.write ('%d,%d,%d\n' % (partner_id, external_item_id, nb_sales))
            
    nosales_fp.close()
    sales_fp.close()

def parse_catalog_file_split_views (filename, output_views_file, output_noviews_file):
    noviews_fp = open (output_noviews_file, 'a')
    views_fp = open (output_views_file, 'a')

    with open (filename,'r') as fp:
        lines = fp.readlines()
        for line in lines:
            try:
                data = json.loads(line)
            except:
                _log('*** ERROR *** Failed to read JSON file %s.  File might be empty.  Exiting.' % json_filename)
                return None
            nb_views = int(data['popularity']['nb_views_30d'])
            external_item_id = int(data['external_item_id_hash_64'])
            partner_id = int(data['partner_id'])
            if (nb_views == 0):
                noviews_fp.write ('%d,%d,%d\n' % (partner_id, external_item_id, nb_views))
            else:
                views_fp.write ('%d,%d,%d\n' % (partner_id, external_item_id, nb_views))
            
    noviews_fp.close()
    views_fp.close()


def process_dir (host):

    hdfs_dir = '/user/recocomputer/bestofs/rich_catalog'
    partners_file = 'lastPartnerTimestamp.txt'
    output_sales_file = 'sales.txt'
    output_nosales_file = 'nosales.txt'
    output_views_file = 'views.txt'
    output_noviews_file = 'noviews.txt'

    if os.path.isfile (output_sales_file):
        os.remove (output_sales_file)
    if os.path.isfile (output_nosales_file):
        os.remove (output_nosales_file)
    if os.path.isfile (output_views_file):
        os.remove (output_views_file)
    if os.path.isfile (output_noviews_file):
        os.remove (output_noviews_file)

    # read list of partners
    partner_ids = list_partners_from_file (partners_file)

    # read output by partner
    data = read_output_by_partner (os.path.join (hdfs_dir, 'outputByPartner'))

    # parse catalog files data
    hdfs_file_list = parse_json_output (data, hdfs_dir, partner_ids)

    # read rich catalog
    for hdfs_file in hdfs_file_list:
        catalog_filename = read_hdfs_catalog_file (hdfs_file)

        parse_catalog_file_split_sales (catalog_filename, output_sales_file, output_nosales_file)
    #    parse_catalog_file_split_views (catalog_filename, output_views_file, output_noviews_file)

    return



if __name__ == "__main__":
    main()

